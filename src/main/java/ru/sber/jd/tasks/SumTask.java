package ru.sber.jd.tasks;

import ru.sber.jd.utils.SumUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class SumTask extends RecursiveTask<Integer> {

    private List<Integer> values;
    Integer result = 0;

    public SumTask(List<Integer> values) {
        this.values = values;
    }

    @Override
    protected Integer compute() {

        if (values.size() == 2) {
            return SumUtils.sum(values.get(0), values.get(1), 1000);
        }


        List<SumTask> subTasks = new ArrayList<>();


        for (int i = 0; i < values.size(); i++) {
            if (i % 2 != 0 && i > 0) {
                List<Integer> mySum = new ArrayList<>();
                mySum.add(values.get(i - 1));
                mySum.add(values.get(i));

                SumTask task = new SumTask(mySum);
                task.fork();
                subTasks.add(task);
            }
        }

        for (SumTask task : subTasks) {
            result += task.join();
        }

        return result;
    }
}
