package ru.sber.jd;

import ru.sber.jd.services.ThreadCalculator;
import ru.sber.jd.tasks.SumTask;
import ru.sber.jd.utils.SumUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Main {


    public static void main(String[] args) throws ExecutionException, InterruptedException {

        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "500");
        Long resultTime = 0L, resultTime1 = 0L, resultTime2 = 0L;
        Integer result = 0, result1 = 0, result2 = 0;

        // пример ForkJoinPool
        long startTime = System.currentTimeMillis();

        List<Integer> values = new ArrayList<>();

        for (int i = 1; i <= 1000; i++) {
            values.add(i);
        }

        result = new ForkJoinPool().invoke(new SumTask(values));

        long endTime = System.currentTimeMillis();
        resultTime = endTime - startTime;



        // пример parallelStream
        long startTime1 = System.currentTimeMillis();


        List<Integer> values1 = new ArrayList<>();
        for (int i = 1; i <= 1000; i++) {
            values1.add(i);
        }


        result1 = values1.parallelStream().reduce((a, b) -> SumUtils.sum(a, b, 1000)).get();

        long endTime1 = System.currentTimeMillis();
        resultTime1 = endTime1 - startTime1;


        //пример Runnable
        long startTime2 = System.currentTimeMillis();

        List<Integer> values2 = new ArrayList<>();

        for (int i = 1; i <= 1000; i++) {
            values2.add(i);
        }

        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < values2.size(); i++) {
            if (i % 2 != 0 && i > 0) {
                Thread otherThread = new ThreadCalculator(values2.get(i),values2.get(i-1));
                otherThread.start();
                threads.add(otherThread);
            }
        }

        for (int i = threads.size() - 1; i >= 0; i--) {
            threads.get(i).join();
            result2 += ((ThreadCalculator) threads.get(i)).getSum();
        }

        long endTime2 = System.currentTimeMillis();
        resultTime2 = endTime2 - startTime2;



        // Programm result
        System.out.println("Результат работы " + result + " Время работы ForkJoinPool " + resultTime);
        System.out.println("Результат работы " + result1 + " Время работы parallelStream " + resultTime1);
        System.out.println("Результат работы " + result2 + " Время работы Runnable " + resultTime2);


    }


}
