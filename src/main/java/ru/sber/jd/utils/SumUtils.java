package ru.sber.jd.utils;

public class SumUtils {


    public static Integer sum(Integer a, Integer b, Integer timeSleep) {
        try {
            Thread.sleep(timeSleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Имя потока: " + Thread.currentThread().getName() + " | Сумма = " + (a + b));

        return a + b;
    }



}
