package ru.sber.jd.services;

import ru.sber.jd.utils.SumUtils;

public class ThreadCalculator extends Thread {


        private Integer a, b, c;

        public ThreadCalculator(Integer a, Integer b) {
            this.a = a;
            this.b = b;
        }

        @Override
        public void run() {
            c = SumUtils.sum(a, b, 1000);
        }

        public Integer getSum() {
            return this.c;
        }

    }
